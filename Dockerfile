#Imagen raiz es la imagen que nos instalamos el primer día.
FROM node

#Carpeta raiz
WORKDIR /apitechu

#copia de archivos añadir todo lo que haya en mi directorio donde estoy posicionados a /apitechu
ADD . /apitechu

#Añadir Volumen
#VOLUME ['/data']

#Exponer puerto es el del contenedor
EXPOSE 3000

#Instalar dependencias
#RUN npm install

#Comando de ejecutar la API (como se hacia en la consola para llamar a la api)
CMD ["npm","start"]
