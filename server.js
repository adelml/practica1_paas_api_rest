var express=require('express'); //inclusión de express
var app=express();
var bodyParser =require('body-parser');
var jwt=require('jsonwebtoken');
var expressJwt=require('express-jwt');
var jwtClave="11082018";
app.use(expressJwt({secret:jwtClave}).unless({path: ["/apitechu/v2/login","/apitechu/v2/users/new","/apitechu/v2/coordenadas" ]}));
//app.use(expressJwt({secret:jwtClave}).unless({path: ["/apitechu/v2/users/new"]}));


//app.use(expressJwt({secret:jwtClave}).unless({path: ["/apitechu/v2/users/:id/Accounts"]}));

var bodyParser =require('body-parser');
app.use(bodyParser.json());
var port=process.env.PORT || 3000; //Puerto para express

var baseMlabURL="https://api.mlab.com/api/1/databases/apitechuaml/collections/";
var requestJson=require('request-json');
var mLabAPIKey = "apiKey=Aw4Z7pEGSWgIdppI44lYToGVRLk1UCk2";

app.listen(port); //ponemos a escuchar
console.log("API escuchando en el puerto " + port);

/*****************************************************************************/
/*                                      FUNCIONES                            */
/*****************************************************************************/

//FUNCION ESCRITURA
function writeUserDataToFile(data){ //en este caso el nombre del fichero entero
  var fs= require('fs');
  var jsonUserData = JSON.stringify(data);// para no guardarlo en binario//string a grabar en el archivo de texto

  fs.writeFile(
    "./usuarios.json", //nombre del fichero de texto  crear
    jsonUserData, //string a grabar en el archivo de texto (en este caso todo el fichero usaurios (data))
    "utf8",
    function (err){
      if (err){
        console.log(err);
      } else {
        console.log("usuario persistido");
      }
    }
  )
}


//FUNCION OBTENCION IBAN
function makeid() {
  var text = "";
  var possible = "0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}


//var mystring = makeid();


//ver: fs.readFile('./archivo1.txt', (error,datos)


//*********************************************************************//
//                GET
//*********************************************************************//


//LEER FICHERO_USER desde MLAB
app.get('/apitechu/v2/users',
function(req,res){
  console.log("GET /apitechu/v2/users");
  httpClient=requestJson.createClient(baseMlabURL);
  console.log("Acceso a Users");

  httpClient.get("user?" + mLabAPIKey,
    function(err,resMLab,body){
      var response=!err?body:{
        "msg":"Error obteniendo usuarios."
      }
      res.send(response);
    }
  )
}
);

//LEER API EXTERNA
app.get('/apitechu/v2/coordenadas',
function(req,res){
  console.log("GET /apitechu/v2/coordenadas");
  httpClient=requestJson.createClient(baseMlabURL);
  console.log("Acceso a Users");

  httpClient.get("coordenadas?" + mLabAPIKey,
    function(err,resMLab,body){
      var response=!err?body:{
        "msg":"Error obteniendo coordenadas."
      }
      res.send(response);
    }
  )
}
);


//LEER FICHERO_Account desde MLAB
app.get('/apitechu/v2/Accounts',
function(req,res){
  console.log("GET /apitechu/v2/Accounts");
  httpClient=requestJson.createClient(baseMlabURL);
  console.log("Cuentas");
  var query=
  httpClient.get("account?"+mLabAPIKey,
    function(err,resMLab,body){
      var response=!err?body:{
        "msg":"Error obteniendo Cuentas."
      }
      res.send(response);
    }
  )
}
);


//LEER FICHERO
app.get('/apitechu/v2/users/:id',
function(req,res){
  console.log("GET /apitechu/v2/users");

  var id =req.params.id; //id que se pasa por parametro!!
  var query = 'q={"id": ' + id+'}';

  httpClient=requestJson.createClient(baseMlabURL);
  console.log("Cliente creado");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body) {
   if (err) {
     var response = {
       "msg" : "Error obteniendo usuario."
     }
     res.status(500);
   } else {
     if (body.length > 0) {
        var response = body[0];
     } else {
       var response = {
         "msg" : "Usuario no encontrado."
       };
       res.status(404);
     }
   }
   res.send(response);
    }
  )
  }
);

//LEER balance
/*app.get('/apitechu/v2/users/:iban/balance',
function(req,res){
  console.log("GET /apitechu/v2/users/balance");
  var iban =req.params.iban;
  //var id =req.params.id; //id que se pasa por parametro!!
  var query = 'q={"iban": ' +iban+' }';
//  var query = 'q={"id": ' + id+' }';
  var querysort = 's={"id_mto":'+1+'}';
  console.log("query" + query);
  httpClient=requestJson.createClient(baseMlabURL);
  console.log("Accceso a Mlab");

  httpClient.get("balance?" + query + "&"+ querysort +"&" + mLabAPIKey,
  function(err, resMLab, body) {
    if (err) {
      var response = {
        "msg" : "Error obteniendo iban."
      }
      res.status(500);
    } else {
           if (body.length > 0) {
               var response = body; //para devolver todooas los moviemienos
         } else {
             var response = {
                       "msg" : "Iban no encontrado." + iban
                       };
               res.status(404);
      }
    }
    res.send(response);
     }
   )
   }
 );*/

 app.get('/apitechu/v2/users/:id/:id_cc/balance',
 function(req,res){
   console.log("GET /apitechu/v2/users/balance");

   var id =req.params.id;
   var id_cc =req.params.id_cc;
   id = Number(id);
   id_cc= Number(id_cc);

   var query = 'q={"id": ' + id+' , "id_cc": ' + id_cc+' }';
  // var query2 = 'q={"id_cc": ' + id_cc+' }';
   var querysort = 's={"id_mto":'+-1+'}';
   var querylim = 'l='+10+'';
   console.log("query" + query);
   httpClient=requestJson.createClient(baseMlabURL);
   console.log("Accceso a Mlab");

   httpClient.get("balance?" + query + "&"+ querysort +"&"+ querylim +"&"+ mLabAPIKey,
   function(err, resMLab, body) {
     if (err) {
       var response = {
         "msg" : "Error obteniendo iban."
       }
       res.status(500);
     } else {
            if (body.length > 0) {
                var response = body; //para devolver todooas los moviemienos
          } else {
              var response = {
                        "msg" : "Iban no encontrado." 
                        };
                res.status(404);
       }
     }
     res.send(response);
      }
    )
    }
  );



app.get('/apitechu/v2/users/:id/Accounts',
function(req,res){
  console.log("GET /apitechu/v2/users/Accounts");

  var id =req.params.id; //id que se pasa por parametro!!
  var query = 'q={"id": ' + id+'}';

  httpClient=requestJson.createClient(baseMlabURL);
  console.log("Accceso a Mlab");

  httpClient.get("account?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body) {
   if (err) {
     response = {
       "msg" : "Error obteniendo id."
     }
     res.status(500);
   } else {
          if (body.length > 0) {
            var response = body; //para devolver todooas las cuentas
        } else {
            var response = {
                      "msg" : "Id no encontrado."
                      };
              res.status(404);
     }
   }
   res.send(response);
    }
  )
  }
);


//*********************************************************************//
//                POST
//*********************************************************************//

/*Nuevo Usuario con nueva cuenta*/
app.post('/apitechu/v2/users/new',
  function(req, res){
    console.log("POST /apitechu/v2/users/new");
    httpClient=requestJson.createClient(baseMlabURL);
    console.log("Cliente creado");
    console.log(req.body);

    var query = 'q={"email": "'+ req.body.email + '","password": "'+ req.body.password + '"}'
    console.log("query" + query);
    console.log("Comprobamos si existe");

    httpClient.get("user?"+ query + "&"  + mLabAPIKey,
      function(err,resMLab,body){
        console.log("body:" + body);
        if (body.length == 0) {
            console.log("El usuario no existe");

            var query = 's={"id":'+-1+'}';
            var query2 = 'l='+1+'';
            console.log("query" + query +"&"+ query2);

            httpClient.get("user?"+ query + "&" + query2 + "&" + mLabAPIKey,
              function(err,resMLab,body){
                console.log("body:" + body);
                if (body.length > 0) {
                      var id = body[0].id;
                      console.log("id:" + id);
                      var idnext = id + 1;
                      idnext = Number(idnext);

                      console.log("Id max encontrado.");
                      console.log(idnext);
                      /*var response = {
                                "msg" : "Id max encontrado." + id
                              };*/

                        var jsonUserData = JSON.stringify({
                                  "id": idnext,
                                  "first_name": req.body.first_name,
                                  "last_name": req.body.last_name,
                                  "email" : req.body.email,
                                  "password" : req.body.password,
                                  "gender" : req.body.gender,
                                  "address":req.body.address,
                                  "nif":req.body.nif,
                                  "phone":req.body.phone,
                                  "country":req.body.country
                                });
                              //var jsonUserData = JSON.stringify(req.body);
                        console.log(jsonUserData);
                        httpClient.post("user?" + "&" + mLabAPIKey,JSON.parse(jsonUserData),
                          function(err, resMLab, body) {
                            if (err) {
                              //console.log(err);
                              var response = {
                                    "msg" : "Usuario no se ha podido insertar."
                                            }
                              res.send(response);
                            } else {
                                    var banco = "0182";
                                    var iban2 = makeid();
                                    var iban3 = makeid();
                                    var iban4 = makeid();
                                    var iban5 = makeid();
                                    var iban = banco + " " +iban2 + " " + iban3 + " "+ iban4 + " "+ iban5;
                                    //var fecha = Date.now();
                                    var fecha = new Date();
                                    fecha = fecha.toDateString();
                                  //  var concepto = "Apertura cuenta ";
                                  //  var cargo = 0;
                                  //  var importe=0;
                                    var jsonUserCuenta = JSON.stringify({
                                             "id": idnext,
                                             "id_cc":1,
                                             "id_mto": 1,
                                              "fecha": fecha,
                                              "iban": iban,
                                              "concepto" : "Apertura cuenta ",
                                              "cargo" : 0,
                                              "Importe" : 0
                                            });
                                          //var jsonUserData = JSON.stringify(req.body);
                                    console.log(jsonUserCuenta);
                                    httpClient.post("balance?" + "&" + mLabAPIKey,JSON.parse(jsonUserCuenta),
                                      function(err, resMLab, body) {
                                        if (err) {
                                          //console.log(err);
                                          var response = {
                                                "msg" : "Usuario no se ha podido insertar."
                                                        }
                                                res.status(400);
                                        } else {
                                              console.log("insertado en balance");
                                              var jsonUserCuenta = JSON.stringify({
                                                     "id": idnext,
                                                     "id_cc":1,
                                                     "iban": iban,
                                                     "Importe" : 0,
                                                     "type_cc" : "Cuenta Nómina"
                                                     });
                                              console.log(jsonUserCuenta);
                                              httpClient.post("account?" + "&" + mLabAPIKey,JSON.parse(jsonUserCuenta),
                                              function(err, resMLab, body) {
                                              console.log(body);
                                              if (err) {
                                              //console.log(err);
                                                  var response = {
                                                       "msg" : "Usuario no se ha podido insertar."
                                                             }
                                                        res.status(400);
                                              } else {
                                                 console.log("insertado en cuenta");
                                                  var response = {
                                                        "msg" : "Usuario insertado"
                                                              }
                                                res.status(201);
                                               }

                                             res.send(response);
                                          }
                                        )
                                        }
                                      }
                                      )
                                    }
                                 }
                               )
                       } else {
                            var response = {
                              "msg" : "Id no encontrado."
                                        };
                              res.status(404);
                      res.send(response);
                   }

               }
             )
      } else {
            var response = {
              "msg" : "El usuario ya existe."
                        };
            res.status(202);
      res.send(response);

       }
      }
     )
    }
  );


  /*Nueva cuenta para clientes*/
  app.post('/apitechu/v2/users/new/account',
    function(req, res){
      console.log("POST /apitechu/v2/users/new/account");
      httpClient=requestJson.createClient(baseMlabURL);
      console.log("Cliente creado");
      console.log(req.body);
      var id =req.body.id;
      id = Number(id);
      var typecc=req.body.typecc;
      console.log("typecc " + typecc);

      var query = 'q={"id": '+ req.body.id + '}'
      var queryS = 's={"id_cc":'+-1+'}';
      var queryL = 'l='+1+'';
      console.log("query " + query);
      console.log("accedemos a account para obetener el id_cc max");

      httpClient.get("account?"+ query + "&" + queryS + "&" +queryL+ "&"  + mLabAPIKey,
        function(err,resMLab,body){
          console.log("body:" + body);
          if (body.length > 0) {
              console.log("acede");
              var id_cc = body[0].id_cc;
              console.log("id_cc:" + id_cc);
              var idccnext = id_cc + 1;
              idccnext = Number(idccnext);

              console.log("Id max calculado: " , idccnext);

              var banco = "0182";
              var iban2 = makeid();
              var iban3 = makeid();
              var iban4 = makeid();
              var iban5 = makeid();
              var iban = banco + " " +iban2 + " " + iban3 + " "+ iban4 + " "+ iban5;
              var fecha = new Date();
              fecha = fecha.toDateString();

              var jsonUserCuenta = JSON.stringify({
                                  "id": id,
                                  "id_cc":idccnext,
                                  "id_mto": 1,
                                  "fecha": fecha,
                                  "iban": iban,
                                  "concepto" : "Apertura cuenta nueva cliente",
                                  "cargo" : 0,
                                  "Importe" : 0
                                    });

                console.log(jsonUserCuenta);
                httpClient.post("balance?" + "&" + mLabAPIKey,JSON.parse(jsonUserCuenta),
                    function(err, resMLab, body) {
                        if (err) {
                          var response = {
                                        "msg" : "Cuenta no se ha podido insertar en balance."
                                        }
                          res.status(400);
                         } else {
                           console.log("insertado en balance");
                           var typecc=req.body.typecc;

                           console.log(typecc);
                           var jsonUserCuenta = JSON.stringify({
                                        "id": id,
                                        "id_cc":idccnext,
                                        "iban": iban,
                                        "Importe" : 0,
                                        "type_cc":typecc,
                                        });
                            console.log(jsonUserCuenta);
                            httpClient.post("account?" + "&" + mLabAPIKey,JSON.parse(jsonUserCuenta),
                                function(err, resMLab, body) {
                                console.log(body);
                                if (err) {
                                                //console.log(err);
                                var response = {
                                            "msg" : "Cuenta no se ha podido insertar en account."
                                               }
                                              res.status(400);
                                } else {
                                     console.log("Insertada cuenta");
                                      var response = {
                                                    "msg" : "Cuenta insertado"
                                                    }
                                        res.status(201);
                                        res.send(response);
                                        }
                                     }
                                  )
                                }
                              }
                            )
                          }else{
                            console.log("error acceso account");
                            var response = {
                                        "msg" : "Error datos"
                                           }
                                          res.status(505);
                            res.send(response);
                                }
                        }
                      )
                      }
                    );


/*********/
//Transferencia

app.post('/apitechu/v2/transferencia',

  function(req, res){
    console.log("POST /apitechu/v2/transferencia");
    httpClient=requestJson.createClient(baseMlabURL);
    console.log("Cliente creado");
    console.log("req.body: " +  req.body);

//datos de entrada:
    var id = req.body.id;
    var id_cc =req.body.id_cc;
    id = Number(id);
    id_cc= Number(id_cc);
    var iban = req.body.iban;
    console.log("id " + id);
    console.log("id_cc" + id_cc);
    console.log("iban " + iban);

    var ibandes = req.body.ibandes;
    var imptran = req.body.imptran;
    imptran = Number(imptran).toFixed(2);
    var concepto = req.body.concepto;
    console.log("ibandes " + ibandes);
    console.log("imptran " + imptran);
    console.log("concepto " + concepto);

    console.log("Recogemos el importe de la cuenta origen");
    var queryG = 'q={"id": ' + id+', "id_cc": ' + id_cc+'}';
    console.log("queryG ",queryG);
    //var queryg = 'q={"iban": ' + iban+'}';

    httpClient.get("account?" + queryG + "&" + mLabAPIKey,
    function(err, resMLab, body) {

      var imp = body[0].Importe;
      imp = Number(imp).toFixed(2);
      var impOrigEnd =  imp - imptran;
      impOrigEnd = Number(impOrigEnd).toFixed(2);
      console.log("imp origen " + imp);
      console.log("importe origen end " + impOrigEnd);

      if (impOrigEnd > 0){
          var query = 'q={"iban": "'+ req.body.ibandes +'"}';

          console.log("Comprobamos si existe cuenta destino");
          console.log("query" + query);
          httpClient.get("account?"+ query + "&"+ mLabAPIKey,
            function(err,resMLab,bodydes){

              console.log("bodydes:" + bodydes);

                  if (bodydes.length > 0) {
                      var iddes = bodydes[0].id;
                      var iddes_cc = bodydes[0].id_cc;
                      console.log("La cuenta destino existe "  + iddes + iddes_cc );

                      var imporDest = bodydes[0].Importe;
                      imporDest = Number(imporDest).toFixed(2);

                      var impdestend = Number(imporDest) + Number(imptran);
                      impdestend = Number(impdestend).toFixed(2);

                      console.log("imptran " + imptran );
                      console.log("imporDest " + imporDest );
                      console.log("impdestend " + impdestend );

                      console.log("Actualizamos tabla account cuenta origen ");
                      var putBody = 'q={"id": '+ id +',"id_cc": '+ id_cc +' }'
                      var putOrig = '{"$set":{"Importe": '+impOrigEnd+'}}';
                      console.log("putBody" + putBody );
                      console.log("putOrig" + putOrig );

                      httpClient.put("account?"+ putBody + "&"+ mLabAPIKey,JSON.parse(putOrig),
                        function(errput,resMLab,bodyacc){
                            console.log("bodyacc 1 " + bodyacc);

                            console.log("Actualizamos tabla account cuenta destino ");

                            var putBodydes = 'q={"id": '+ iddes +' ,"id_cc": '+ iddes_cc +'}';
                            var putDes = '{"$set":{"Importe":'+impdestend+'}}';
                            console.log("putBodydes" + putBodydes );
                            console.log("putDes" + putDes );

                              httpClient.put("account?"+ putBodydes + "&"+ mLabAPIKey,JSON.parse(putDes),
                              function(errput,resMLab,bodyacc){
                              if (errput) {
                                  console.log("Error actualizando cuenta destino, revocamos origen");
                                  var putOrig = '{"$set":{"Importe":'+imp+'}}';

                                  httpClient.put("account?"+ putBody + "&"+ mLabAPIKey,JSON.parse(putOrig),
                                  function(errput,resMLab,bodyacc){ })
                                  var response = "err000";
                                  res.send(response);
                                  res.status(500);
                                } else {
                                      console.log("insertamos en  tabla balance cuenta destino ");
                                      var query1 = 'q={"id":'+iddes+',"id_cc":'+iddes_cc+'}';
                                      var query = 's={"id_mto":'+-1+'}';
                                      var query2 = 'l='+1+'';
                                      console.log("query" + query +"&"+ query2);

                                      httpClient.get("balance?"+ query1 + "&"+ query + "&" + query2 + "&" + mLabAPIKey,
                                        function(err,resMLab,body){
                                          console.log("body:" + body);
                                          var id_mto = body[0].id_mto;
                                          console.log("id_mto:" + id_mto);
                                          var id_mtonext = id_mto+ 1;
                                          id_mtonext= Number(id_mtonext);
                                          console.log("id_mto max encontrado.");
                                          console.log(id_mtonext);
                                          var fecha = new Date();
                                          fecha = fecha.toDateString();
                                          console.log(fecha);

                                          var jsonCuentaDest = JSON.stringify({
                                                          "id": iddes,
                                                          "id_cc": iddes_cc,
                                                          "id_mto":id_mtonext,
                                                           "fecha": fecha,
                                                           "iban": ibandes,
                                                           "concepto" : concepto,
                                                            "cargo" : imptran,
                                                            "Importe" : impdestend
                                                             });
                                          httpClient.post("balance?"+ "&"+ mLabAPIKey,JSON.parse(jsonCuentaDest),
                                            function(errpost,resMLab,bodybal){
                                            if (errpost) {
                                              console.log("Error insertando en cuenta destino, revocamos actualizaciones");
                                              var putBody = 'q={"id": '+ id +',"id_cc": '+ id_cc +'}';
                                              var putOrig = '{"$set":{"Importe": "'+imp+'"}}';
                                              httpClient.put("account?"+ putBody + "&"+ mLabAPIKey,JSON.parse(putOrig),
                                              function(errput,resMLab,bodyacc){});

                                              var putBodydes = 'q={"id": '+ iddes +',"id_cc": '+ iddes_cc +'}';
                                              var putDes = '{"$set":{"Importe":"'+imporDest+'"}}';
                                              httpClient.put("account?"+ putBodydes + "&"+ mLabAPIKey,JSON.parse(putDes),
                                              function(errput,resMLab,bodyacc){});

                                              var response = "err000"
                                              res.send(response);
                                              res.status(500);

                                              } else {
                                                  console.log("bodybaldestino " + bodybal);
                                                  console.log("Insertamos cuenta origen");
                                                  var query1 = 'q={"id":'+id+',"id_cc":'+id_cc+'}';
                                                  var query = 's={"id_mto":'+-1+'}';
                                                  var query2 = 'l='+1+'';
                                                  console.log("query" + query +"&"+ query2);

                                                  httpClient.get("balance?"+ query1 + "&"+ query + "&" + query2 + "&" + mLabAPIKey,
                                                    function(err,resMLab,body){

                                                      var id_mto = body[0].id_mto;
                                                      console.log("id_mto:" + id_mto);
                                                      var id_mtonext = id_mto+ 1;
                                                      id_mtonext = Number(id_mtonext);
                                                      console.log("id_mto max encontrado origen.");
                                                      console.log(id_mtonext);
                                                      var fecha = new Date();
                                                      var cargo= -1 * imptran
                                                      fecha = fecha.toDateString();
                                                      console.log("id"+ id);
                                                      console.log("iban"+ iban);
                                                      console.log("concepto" + concepto);
                                                      console.log("cargo" + cargo);
                                                      console.log("Importe" + impOrigEnd);
                                                      var jsonCuentaOrig = JSON.stringify({
                                                                  "id": id,
                                                                  "id_cc": id_cc,
                                                                  "id_mto":id_mtonext,
                                                                   "fecha": fecha,
                                                                   "iban": iban,
                                                                   "concepto" : concepto,
                                                                    "cargo" : cargo,
                                                                    "Importe" : impOrigEnd
                                                                     });
                                                      httpClient.post("balance?"+ "&"+ mLabAPIKey,JSON.parse(jsonCuentaOrig),
                                                      function(errpost,resMLab,bodybal){
                                                      if (errpost) {
                                                          console.log("Error insertando en cuenta Origen,revocamos actualizaciones");

                                                          var putOrig = '{"$set":{"Importe":"'+imp+'"}}';
                                                          httpClient.put("account?"+ putBody + "&"+ mLabAPIKey,JSON.parse(putOrig),
                                                          function(errput,resMLab,bodyacc){});

                                                          var putBodydes = 'q={"id": '+ iddes +',"id_cc": '+ iddes_cc +'}';
                                                          var putDes = '{"$set":{"Importe":"'+imporDest+'"}}';
                                                          httpClient.put("account?"+ putBodydes + "&"+ mLabAPIKey,JSON.parse(putDes),
                                                          function(errput,resMLab,bodyacc){});

                                                          var response = "err101";
                                                          res.send(response);
                                                          res.status(500);
                                                        } else {console.log("Insertamos cuenta Origen ok ");

                                                                var response1 = JSON.stringify({
                                                                    "msg" : "Oktransf",
                                                                    "id":id,
                                                                    "id_cc":id_cc});
                                                                var response = response1;
                                                                console.log("response1", response);
                                                              }
                                                        //res.send(response);

                                                          });
                                                        });
                                                      }
                                                    })
                                                  })
                                                }
                                            })
                                          })
                          //  res.send(response);
                        var response1 = JSON.stringify({ "msg" : "Oktransf2",
                                                        "id":id,
                                                        "id_cc":id_cc});
                        var response = response1;
                        console.log("response2", response);
                        res.send(response);

                    }else{console.log("No existe cuenta destino ");
                      var response = { "msg" : "No existe cuenta destino"};
                      res.status(404);
                      res.send(response);
                          }

                      })

                }else{
                console.log("Sin fondos ");
                var response = { "msg" : "Sin fondos"};
                res.send(response);
                //res.status(400);
                }

              })
/*aqui el fin del get*/

          });

//********************LOGIN LOGOUT*************************//
/********************************************/
// LOGIN LOGOUT EN BBDD//
/*******************************************/

app.post('/apitechu/v2/login',
function(req,res){
  console.log("POST /apitechu/v2/login");

  var query = 'q={"email": "'+ req.body.email + '","password": "'+ req.body.password + '"}'

  console.log("query" + query);

  httpClient=requestJson.createClient(baseMlabURL);
  console.log("Cliente creado");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body) {
   if (err) {
      var response = {
       "msg" : "Error obteniendo usuario." + err
     }
     res.send(response);
     res.status(500);
   } else {
      console.log("else del login no error");

      if (body.length > 0) {
        /*var response = body[0];*/
        var queryput ='q={"id" :'+body[0].id +'}';
        var putBody = '{"$set":{"logged":true}}';
        console.log(queryput);

        httpClient.put("user?" + queryput + "&" + mLabAPIKey,JSON.parse(putBody),
          function(errorput, resMLab, bodyput) {
            if (errorput) {
              console.log("errorput");
              var response = {
                "msg" : "Error PUT"
                          }
              } else   {
                console.log("No errorput");
                //var response = body[0];
                var token=jwt.sign({
                  id:body[0].id
                  },jwtClave);
                  var response = {
                    "token" : token,
                    "id":body[0].id,
                    "first_name":body[0].first_name,
                    "last_name":body[0].last_name
                              }
                console.log(token);
                console.log(response);
                }
              res.send(response);
             }
           )
       } else {
       var response = {
         "msg" : "Usuario no encontrado."
       };
          res.status(404);
          res.send(response);
          }
        }
    /*  res.send(response);*/
      }
    )
  }
);


//****BOTON LOGOUT*****//
//https://api.mlab.com/api/1/databases/apitechuaml/collections/user?q={%22id%22:1}&apiKey=Aw4Z7pEGSWgIdppI44lYToGVRLk1UCk2

app.post('/apitechu/v2/logout',
function(req,res){
  console.log("POST /apitechu/v2/logout");

  var query = 'q={"id": '+ req.body.id + '}'

  console.log("query:" + query);

  httpClient=requestJson.createClient(baseMlabURL);
  console.log("Cliente creado");
  console.log(req.body.id);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body) {
   if (err) {
     var response = {
       "msg" : "Error obteniendo usuario."
     }
     res.send(response);
     res.status(500);
   } else {
      console.log("else logout");
      console.log(req.body);

     if (body.length > 0) {
       var response =
       {
         "msg" : "Usuario encontrado." + body[0].id
       }

        var queryput ='q={"id" :'+body[0].id +',"logged":true}';
        var putBody = '{"$unset":{"logged":"true"}}';

        console.log("queryput" , queryput);
        console.log("putBody",putBody);

        httpClient.put("user?" + queryput + "&" + mLabAPIKey,JSON.parse(putBody),
          function(errorput, resMLab, bodyput) {
            console.log(bodyput);
            if (errorput) {
              var response = {
                "msg" : "Error PUT"
               }
             } else  {
                if (bodyput.n == 1) { //la salida de la query set devuelve 1 o 0
                  var response = {
                     "msg" : "Usuario deslogado correctamente"
                     }
                   }else{
                     var response = {
                      "msg" : "Este usuario no estaba logado"
                      }
                      res.status(202);
                    }
              }
              res.send(response);
            }

          )


       } else {
       var response = {
         "msg" : "Usuario no encontrado." + req.body.id
       }
       res.status(404);
       res.send(response)
     }
     /*res.send(response);*/
    }
   }
  )
 }
);
//*********************************************************************//
//                DELETE
//*********************************************************************//


app.delete('/apitechu/v1/users/:id',
  function(req,res) {
    //Do stuff
    console.log("DELETE /apitechu/v1/users/:id");
    var users = require('./usuarios.json');
    users.splice(req.params.id - 1, 1);
    writeUserDataToFile(users);
    res.send(
      {
        "msg": "usuario borrado con éxito"
      }
    );
  }
);
